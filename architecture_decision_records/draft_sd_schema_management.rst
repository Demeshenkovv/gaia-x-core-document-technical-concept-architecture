ADR-XXX: Managing Self-Description Schemas in the Federated Catalogue
=====================================================================

:adr-id: XXX
:revnumber: 1.0 
:revdate: 2021-04-27
:status: draft
:author: Self-Description OWP, Federated Catalogue OWP
:stakeholder: Self-Description OWP, Catalogue OWP

Summary
-------

For the purpose of validating Self-Descriptions, the Federated Catalogue needs to know the Schema to validate against.
Gaia-X develops an extensible hierarchy of Schemas that define the terms used in Self-Descriptions.
Some Schemas are standardized by the Gaia-X AISBL and must be supported by any Catalogue, but it should be possible to create additional ones specific to an application domain, an Ecosystem, Participants in it, or Assets offered by these Participants.
Schemas have the same format as Self-Descriptions, i.e., they are graphs in the RDF data model, serialized as JSON-LD.
A Schema may define terms (classes, their attributes, and their relationships to other classes) in an Ontology.
If it does, it must also define Shapes to validate instances of the Ontology against.
A Schema may also introduce a Controlled Vocabulary of attribute values intended to be reused.

Context
-------

Class: 
==
Schema: Model with several classes, relations and attributes.
==
Vocabulary: 
==
Ontology: 

Alternative Technologies
~~~~~~~~~~~~~~~~~~~~~~~~

Decision Statements
-------------------

Self-Descriptions refer to one or several schemas via a "isA" relation.

Every schema consists of classes and classes define attributes and relations to other Self-Descriptions.

Only attributes from a class can be used for the Self-Descriptions.

The Self-Description classes form an extensible inheritance hierarchy.
For example geo-location for some Nodes.

The Self-Description classes are versioned according to the Semantic Versioning conventions.

GAIA-X provides and maintains a set of core Self-Description classes.

The schema hierarchy can be extended with specialized classes, for example for a domain-specific ecosystem.

GAIA-X defines requirements and processes to accept and distribute new Self-Description classes and updates thereof.
- Option 1: Central repo by the GAIA-X Foundation for schemas
- Option 2: Ecosystems / private catalogues can have additional schema sources

RDF and SHACL are selected as technologies for schema description and enforcement.
- RDF: Data model "behind" JSON-LD
- SHACL: Validation of RDF against a schema independently from the serialization technology

Consequences
------------

ADR References
--------------

* ADR-001 JSON-LD

External References
-------------------

* [JSON-LD] JSON-LD 1.1 - A JSON-based Serialization for Linked Data,
  https://www.w3.org/TR/json-ld11
* [RDF] RDF 1.1 Concepts and Abstract Syntax,
  https://www.w3.org/TR/rdf11-concepts/
* [SHACL] Shapes Constraint Language (SHACL),
  https://www.w3.org/TR/shacl/
