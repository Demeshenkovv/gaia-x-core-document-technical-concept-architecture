Architecture Decision Records
-----------------------------

.. This file contains the ADR that shall be included
.. in the architecture document

.. toctree::
   :maxdepth: 2

   architecture_decision_records/001_json_ld
   architecture_decision_records/002_rest