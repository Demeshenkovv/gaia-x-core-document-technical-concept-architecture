# GAIA-X Architecture Documentation

This repository contains the architecture documentation for GAIA-X.

The repository is organized into the following folders:

## Architecture Document

The Architecture Document describes the current common understanding of the Architecture.

The output is published here <https://gaia-x.gitlab.io/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/>

### Local development

1. Create a python3 virtual environment
2. `pip install -r requirements.txt`
3. `mkdocs serve -a 0.0.0.0:8000`
4. `mkdocs build`

Generated files are in `public/`

## Architecture Decision Records

Architecture Decision Records (ADR) document important decisions that were proposed and accepted for GAIA-X.

The ADR are listed in in appendix of the Architecture Document.

