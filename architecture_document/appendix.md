# Appendix

## A1

Examples of Attribute Categories per Self-Description in Gaia-X are
discussed in Appendix A.

- **Providers**: Every Provider of Service Offerings has to be
  registered as Provider and thus requires a Self-Description. The
  categories comprise identity, contact information, certification.

- **Nodes:** Self-Descriptions of Nodes describe relevant functional
  and non-functional attributes of Nodes as described in Section
  "Basic Architecture Elements". The Attribute Categories comprise
  availability, connectivity, hardware, monitoring, physical security
  and sustainability.

- **Software Assets:** Self-Descriptions of Software Assets describe
  Software Assets as defined in the [Conceptual Model](conceptual_model.md).
  Attribute Categories for Software Assets are still under discussion
  and are not yet finalized.

- **Consumers (optional)**: Self-Descriptions of Consumers are
  optional, but may be required for accessing critical Data Assets
  and/or specific domains. Attribute categories for Consumers are
  still under discussion and are not yet finalized.

## A2

![](figures/federated-identity-model.png)

**Operational example Federated Trust Model**

0.  A Visitor accesses the Gaia-X Federated Trust, browses the Gaia-X
    Federated Catalogue and starts a Service search query. A list with
    possible services matching the service search criteria will be
    displayed to the Visitor.

1.  The Provider entity registers in Gaia-X. One of the mandatory fields
    is the input to the Identity System. An Identity System must confirm
    the Identity of the Provider.

2.  Existing Identifiers will be enabled for Gaia-X usage. Result: The
    Provider is verified and registered in Gaia-X. The Provider is able
    to register a Service in the Gaia-X Federated Catalogue, it is
    generated during Service Self-Description creation. The registered
    Service will be published to the Gaia-X Federated Catalogue and is
    publicly available.

3.  A Consumer registers in Gaia-X. One of the mandatory fields is the
    input to the Identity System. An Identity System must confirm the
    Identity of the Consumer and can be verified itself by Gaia-X.
    Existing Identifiers will be enabled for Gaia-X usage. Result: The
    Consumer is verified and registered in Gaia-X.

4.  The registered Consumer contacts the Service Provider to order a
    specific Service.

5.  The Provider AM checks the trustworthiness of the Consumer. The
    Gaia-X Federated Trust Component is used to check the Identity via
    the Identity System. The Gaia-X Federated Trust Component is used to
    verify the Service Access (e.g., required certifications of the
    Consumer to access health data).

    a.  Deny/Grant Access

    b.  Deny: The Provider AM will provide the result to the Consumer.

6.  Grant: The Provider AM will trigger the service orchestration engine
    to create the Service Instance for the Consumer (= Service
    Instantiation process). The Service Provider will forward the
    Service Instance Details to the Consumer.

7.  The Consumer is now able to use the requested Service Instance. The
    Provider AM will check/verify for each access the identity of the
    Consumer using the Federated Trust Component to guarantee that the
    Consumer attributes matches the required ones (see step 6/7).

8.  The Consumer can offer - outside of the Gaia-X ecosystem - Services
    to their End-Users (not part of Gaia-X). These external offerings
    can rely on Gaia-X Service instances or can be enriched by data from 
    Gaia-X Services.


## A3

This appendix presents minimal core versions of central Gaia-X concepts.
That includes mandatory attributes, datatypes, and cardinalities for the core concepts of Participant, Provider, Consumer, Service Offering, Asset, Data Asset, Software Asset, Node, and Interconnection:


### Participant

| Attribute     | Possible Datatype(s)                      | Cardinality   |
| ------------- |:-----------------------------------------:| -------------:|
| legal name    | [xsd:string](http://www.w3.org/2001/XMLSchema#)                                | 1..1          |
| legal address | vcard:Address                             | 1..1          |
| web address   | [xsd:anyURI](http://www.w3.org/2001/XMLSchema#anyURI)                                | 1..*          |
| contact       | [vcard:Agent](http://www.w3.org/2006/vcard/ns#), [foaf:Person](http://xmlns.com/foaf/0.1/), [schema:Person](http://schema.org/), [ids:Person](https://w3id.org/idsa/core/Person)   | 1..*          |
| parent_entity | gax-participant:Participant | 0..1          |

#### Provider / Consumer

Same as Participant. These roles are assigned implicitly once they
provide/consume at least one Asset or Service Offering.

### Service Offering

| Attribute     | Possible Datatype(s)      | Cardinality   |
| ------------- |:-------------------------:| -------------:|
| name          | [xsd:string](http://www.w3.org/2001/XMLSchema#)                | 1..1          |
| description   | [dct:description](http://purl.org/dc/terms/description)           | 1..1          |
| provided_by   | [gax-participant:Provider](https://gaia-x.gitlab.io/gaia-x-community/gaia-x-self-descriptions/participant/participant.html#Provider)  | 1..*          |

#### Asset

| Attribute     | Possible Datatype(s)      | Cardinality   |
| ------------- |:-------------------------:| -------------:|
| name          | [xsd:string](http://www.w3.org/2001/XMLSchema#)                | 1..1          |
| description   | [dct:description](http://purl.org/dc/terms/description)           | 1..1          |
| owned_by      | [foaf:Person](http://xmlns.com/foaf/0.1/)  | 1..*          |

#### Data Asset / Software Asset

Data Asset and Software Asset are subclasses of Asset that do not require additional mandatory attributes.

#### Resource

The mandatory attributes are:

| Attribute     | Possible Datatype(s)      | Cardinality   |
| ------------- |:-------------------------:| -------------:|
| location      | [dct:location](http://purl.org/dc/terms/location)              | 1..1          |
| jurisdiction  | [dct:location](http://purl.org/dc/terms/location)              | 1..1          |
| provided_by   | [gax-participant:Participant](http://w3id.org/gaia-x/participant#Provider)  | 1..*          |

#### Interconnection

Interconnection is a subclass of Asset. In addition to the mandatory attributes of the Asset class, an Interconnection has the following mandatory attributes:

| Attribute         | Possible Datatype(s)      | Cardinality   |
| ------------------|:-------------------------:| -------------:|
| connected_node    | [gax-node:Node](http://w3id.org/gaia-x/node#Node)             | 2..*          |
