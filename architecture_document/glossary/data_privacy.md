## Data Privacy

Data Privacy is defined according to ISO/TS 19299:2015, 3.32 as rights and obligations of individuals and organizations with respect to the collection, use, retention, disclosure and disposal of personal information


### references
- ISO/TS 19299:2015, 3.32
