## Ecosystem

An Ecosystem enables value creation by autonomous, loosely coupled actors via both cooperation and competition.

### alias
- Federation

### references

adapted from Moore, J.F. 1998. The Rise of a New Corporate Form. Washington Quarterly. Vol. 21(1), pp. 167-181.
