## Onboarding and Accreditation Workflow 

The onboarding and accreditation workflow is a Federation Service of the category Compliance and concerns the initial onboarding and accreditation of GAIA-X Participants.

### alias
- OAW

### references
- Federation Services Specification GXFS
