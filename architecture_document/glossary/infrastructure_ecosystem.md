## Infrastructure Ecosystem

An Infrastructure Ecosystem is a loose set of actors who provide or consume storage, computing and network capacities.
