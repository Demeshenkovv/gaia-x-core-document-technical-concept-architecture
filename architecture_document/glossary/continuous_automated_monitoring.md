## Continuous Automated Monitoring 

Process that automatically gathers and assesses information about the compliance of Gaia-X services, with regard to the Gaia-X Policy Rules and Architecture of Standards.

### alias
- CAM
