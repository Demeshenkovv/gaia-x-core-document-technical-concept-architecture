## Catalogue

A Catalogue is an instance of the [Federation Service](#federation-services) [Federated Catalogue](#federated-catalogue) and presents a list of [Service Offerings](#service-offering) available.

Catalogues are the main building blocks for the publication and discovery by a [Participant's](#participant) of [Service Offering's](#service-offering) [Self-Descriptions](#self-description).

### alias
- Gaia-X Catalogue
