## Self-Description

A Self-Description expresses characteristics of an [Asset](#asset), [Resource](#resource), [Service Offering](#service-offering) or [Participant](#participant) and describes properties and [Claims](#claim) and are linked to the Identifier.

### alias
- Gaia-X Self-Description
