## Node

A Node represents a computational or physical entity that hosts, manipulates, or interacts with other computational or physical entities.

A Node can contain other Nodes as sub-nodes so that a hierarchy of Nodes is established.

### references
- Archimate 3.1 (2019)
