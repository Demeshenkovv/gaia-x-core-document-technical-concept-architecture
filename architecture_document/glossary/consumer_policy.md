## Consumer Policy

A Consumer Policy is a [Policy in a technical sense](#policy-technical) that describes a [Consumer's](#consumer) restriction of their requested [Assets](#asset) and [Resources](#resource).

### alias
- Search Policy
