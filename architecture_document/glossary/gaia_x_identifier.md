## Gaia-X Identifier

One unique attribute used to identify an entity within the Gaia-X context and following the Gaia-X format.
