#!/bin/sh

curdir=`dirname $0`
echo $curdir

echo "# Glossary" > $curdir/../glossary.md

for f in $curdir/*.md; do
    echo $f
    echo "" >> $curdir/../glossary.md
    echo "" >> $curdir/../glossary.md
    echo "---" >> $curdir/../glossary.md
    echo "" >> $curdir/../glossary.md
    cat $f >> $curdir/../glossary.md
done
