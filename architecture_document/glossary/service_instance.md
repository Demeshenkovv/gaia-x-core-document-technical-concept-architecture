## Service Instance

A Service Instance is the instantiation of a [Service Offering](#service-offering) at runtime, strictly bound to a version of a [Self-Description](#self-description). The Service Instance has a unique Identity and can be composed of one or more atomic building blocks which must be identifiable as they are associated with a [Service Subscription](#service-subscription).
