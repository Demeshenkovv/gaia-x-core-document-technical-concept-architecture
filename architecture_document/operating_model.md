# Gaia-X Operating Model

Gaia-X in its unique endeavor must have an operating model enabling a wide adoption from small and medium-sized enterprises up to large organisations, including highly regulated markets, to be sustainable and scalable.

To achieve the above statements, a non-exhaustive list of Critical Success Factors (CSFs) includes:

1. The solution must provide clear and unambiguous added values to the Participants.
2. The solution must have a transparent governance model with identified accountability and liability.
3. The solution must be easy to use by its Participants.
4. The solution must be financially sustainable for the Participants and the Gaia-X Association.

## Decentralized Autonomous Organization

Decentralized Autonomous Organization[^dao], DAO, is a type of governing model where:

- There is no central leadership.
- Decisions are made by the community's members.
- The regulation is done by a set of automatically enforceable rules on a public blockchain whose goal is to incentive its community's members to achieve a shared common mission.
- The organization has its own rules, including for managing its own funds.

[^dao]: <https://blockchainhub.net/dao-decentralized-autonomous-organization/>

## Gaia-X Association roles

Based on the objective and constraints to achieve those objectives, Gaia-X Association is creating a Gaia-X DAO.

The Gaia-X Association, Gaia-X European Association for Data and Cloud AISBL, has several defined roles, to:

1. Provide a decentralized network with smart contract functionality, via a Gaia-X blockchain.
2. Provide a Gaia-X token enabling transactions on the decentralized network. The token will be based on an EUR stablecoin[^stableeur].
3. Provide the minimal set of rules to implement interoperability among Participants and Ecosystems.
Those rules include how to validate Self-Descriptions, how to issue Labels, how to discover and register on the Gaia-X network another community's Ecosystems and Participants. The rules will be enforced via smart contracts and oracles[^oracles].
4. Provide and operate a decentralized and easily searchable catalogue[^OP].
5. Provide and maintain a list of Self-description URIs violating Gaia-X membership rules. This list must be used by all Gaia-X Catalogue providers to filter out inappropriate content.

[^stableeur]: example are `EURS` and `sEUR`.
[^oracles]: Various oracles can be implemented, include decentralized ones with <https://chain.link/>
[^OP]: Example of decentralized data and algorithms marketplace <https://oceanprotocol.com/>

## Decentralized Consensus algorithms

There is a wide range of existing consensus algorithms[^proofofx]. Among the top, we have `Proof of Work` used for example by Bitcoin. It's very energy consuming. We have `Proof of Stake` used by [Tezos](https://tezos.com/) which requires a minimal set of stakes, i.e. tokens, to become a validator.

Finally, and for Gaia-X, the proposal is to use `Proof of Authority`, with all members of the Board of Directors operating a blockchain node. This is however not a final decision and open for improvement with newer algorithms such as `Proof of Participation`[^PoP].

[^proofofx]: [Study of Blockchain Based Decentralized Consensus Algorithms - DOI 10.1109/TENCON.2019.8929439](http://scis.scichina.com/en/2021/121101.pdf)
[^PoP]: <https://zoobc.how/?qa=92/what-is-proof-of-participation-in-simple-words>

## Interoperability rules

Gaia-X participants which agree to a specific set of additional rules or scope may constitute an Ecosystem with its own governance (e.g. Catena-X).

Individual Ecosystems can register themselves in the Gaia-X Ecosystem under the condition that they use the Gaia-X Architecture and conform to Gaia-X requirements.

### Interoperability criteria

| Criteria | Mandatory | Rules                                                |
|----------|-----------|------------------------------------------------------|
| Base     | Yes       | <ul><li>Must use Gaia-X Self-description format and ontology</li><li>Must provide public access to its raw Self-Description files</li><li>Must use Verifiable credentials</li></ul> |
| Infra    | No        | Must use Gaia-X compliant composition format         |
| Data     | No        | Must use Gaia-X compliant data description format    |
| Identity | No        | Must use Gaia-X compliant Identity methods           |
| Access   | No        | Must use Gaia-X compliant digital rights and access descriptions |
| Usage    | No        | Must use Gaia-X compliant usage policy description[^drm] |

[^drm]: Could be enforced by Digital Rights Management and <https://www.openpolicyagent.org/> or similar.  

Using this mechanism, a `Provider` from one Ecosystem can have their `Service Offering` made available to other Ecosystems. Other Ecosystems may include those `Service Offering`s in their catalogues depending on their internal rules and the interoperability level.

Not all interoperability levels are mandatory to enable the inclusion of existing Ecosystems. The mandatory levels are defined by the Gaia-X DAO.

#### Base layer

The base layer must enable multiple signatures with a single Self-Description.

## Decentralized Catalogue

The Gaia-X Association will provide a decentralized Catalogue which will enable fast and transparent access to Providers and Service Offers, including Infrastructure, Data and Algorithms offered.

Any Ecosystem can extend this Catalogue by registering their own Self-Description storage URI in the Gaia-X blockchain.
This creates a `Catalogue of Catalogues` or more precisely a `ledger of Self-description directories`.

Any Ecosystem can create their specific Catalogue out of the decentralized published Self-Descriptions.

### Data curation

By offering transparent access to structured and verifiable Service-Descriptions, plus visibility on Service Instance consumption, the Participants can extrapolate about the data quality.

Other metadata, such as using [Great Expectations](https://github.com/great-expectations/great_expectations) can be enforced at the Data interoperability layer to promote a Data quality score.
