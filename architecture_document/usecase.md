# Gaia-X Participant Use Cases

The goal of this section is to illustrate how the Consumers, Federators
and Providers described in the conceptual model can appear. Therefore,
different actors are considered in the role of Consumer and Provider.
This section focuses on the most typical kinds of actors and the list is
not exhaustive.

## Provider Use Cases

This section describes typical kinds of actors that have the Provider
role in Gaia-X. This includes cloud service providers, data providers or
providers of Software Assets as well as Interconnection service
providers.

### Cloud Service Provider

This section focuses on cloud service providers in the Provider role. A
possible service model can be Infrastructure as a Service (IaaS), Platform as a Service (PaaS)
or Software as a Service (SaaS) providers. The deployment model explicitly includes
private clouds, public clouds, edge and hybrid clouds. As standardization
is crucial to achieve interoperability and portability, Gaia-X builds
on existing standards. Demanding adherence to certain standards goes
along with several challenges for potential Providers.

### Data Provider

When offering data via Gaia-X, the Data Sovereignty Services offer the
opportunity to provide Data Assets with attached usage control
mechanisms. This means that data provisioning and monitoring on the
usage of data is enabled. Furthermore, the Consumer can also define
Policies, which present obligations for the Provider. This could, as an
example, mean that only data obtained in a certain jurisdiction should
be transmitted. The data can be used for different applications and
proceedings, including training data for artificial intelligence
applications. Data Sovereignty technologies provide transparency for the
data providers about where their data has been processed and under which
conditions.

### Software Asset Provider

Gaia-X offers the opportunity to provide Software Assets that are
data-intensive and use advanced technology approaches, such as
artificial intelligence and big data. They can be offered via the
Federated Catalogues and be provided with certain policies that specify
the obligations for the execution of the Service Instance of that
Software Asset, e.g., only in a certain jurisdiction or for a restricted
period. Software Assets also especially address to Start-Ups or small
and medium enterprises. They obtain the opportunity to provide their services to a broad mass of Gaia-X stakeholders following certain
Compliance regulations and standards, but also provide easy access to
other complementary services being provided via Gaia-X.

### Interconnection Service Provider

In addition to so-called "Best Effort" services (e.g., basic internet
connectivity as part of networking between different Providers), Gaia-X
also provides the possibility to offer more elevated Interconnection
services that exhibit special characteristics such as guarantees of
bandwidth and latency or security-related settings. Like other Service
Offerings, Interconnection Service Offerings will be listed in a
Catalogue. Among others, Interconnection services will compromise the
existing services of internet service providers, internet exchange
points, or cloud service providers such as Network as a Service (NaaS).

## Consumer Use Cases

This section describes different Gaia-X Consumer scenarios, where the
Consumer can obtain different roles. Therefore, the typical role of
Cloud Service Consumers, Data Consumers, Consumers of combined services
and the End-Users of services are described.

### Cloud Service Consumer

The consumption of cloud services via Gaia-X, referring to those who
follow Gaia-X standards and Compliance, increases transparency for
the Consumer. It lowers the barrier to adapt different cloud services
and reduces the risk of lock-in effects. Gaia-X offers the option for
service composition, which also enables the use of cloud-native
services. Furthermore, service composition can be used to build a
customized service package that covers different aspects and Providers,
without binding to a single Provider. These aspects will
facilitate the adoption of cloud services, especially for Small and
Medium Enterprises. They will easily obtain a transparent overview about
cloud services following Gaia-X Policy Rules and be sure to use trustful
services compliant with privacy and security standards. Furthermore, other
customized services may appear, based upon cloud service composition.
Consumers will keep control over their Digital Sovereignty and ensure that 
their trade secrets remain undisclosed.

### Data Consumer

A Consumer of a Data Asset in Gaia-X can be certain that the consumption
takes place in a compliant way where transparency about the Provider is
given. Therefore, the Self-Description and Certification of the Data
Provider creates trust and transparency. Using existing standards for
sovereign data sharing enables further trust and builds on established
processes. Beyond that, defining search policies enables Consumers to
set up specific criteria which a potential Provider needs to fulfil. Gaia-X
also eases the processing and offering of resulting products or
services, so that it accompanies all following steps in the data value
chain. Overall, Gaia-X lowers the entry barriers for Consumers of data
by creating trust in data offerings. Data-related standards within and
across domains make data more accessible and will leverage data sharing
also for small and medium enterprises.

### Consumer of Combined Service

Gaia-X offers the opportunity to combine different services and create
bundles. Consumers of these bundles can be sure that all elements are
Gaia-X compliant and that there is transparency about each involved
actor. Consumers can also create service combinations themselves, by
selecting suitable building blocks from a Catalogue. Here, the Catalogue
and the Compliance levels offer the opportunity to make different
building blocks comparable and visible.

### Consumer of High-Performance Computing Services

As Gaia-X is open for a broad range of various Providers while at the
same time being bound to strict Compliance rules, it provides the
opportunity to address the area of high-performance computing.
Specifically, Federation Services with Identity Management provide
tangible benefits in this area: high-performance computing is often used in the
academic sector with independent Identity federations on national and/or
international levels. These are not often suitable for industrial
applications, e.g., in the form of consortia. Gaia-X could support such use
cases by providing standards for service definitions as well as
solutions to ensure interoperable service compositions which span 
sectors. Furthermore, Gaia-X strives to facilitate easy access and the general
fostering of a collaborative Ecosystem, which also benefits all
stakeholders of the high-performance computing use cases. The Federated
Catalogues make the availability of high-performance computing
transparent and can enable even small businesses to have lower barriers to entry.

### End-User of Data and Cloud Services

The underlying Compliance and policy mechanisms enable the trust of
End-Users in Gaia-X-based end-products or services. This increases the
willingness to use a new service or to expand its application. As Gaia-X
refers to the infrastructure and underlying B2B-relations between
different actors, the End-User will not necessarily recognize that a
Service Instance is based on Gaia-X. The End-User also does not have to be a
Gaia-X Participant or undergo any Certification processes.

## Federator Use Cases

### Federator of a (domain-)specific Gaia-X Ecosystem 

A Federator focusing on a domain-specific Ecosystem provides the
Federation Services according to the specific needs of this domain. The
Compliance to Gaia-X must be fulfilled and Federation Services should
comply with, or be based upon, open source Federation Services software. The
domain-specific Ecosystem may include, for example, domain-specific
Catalogues, additional trust mechanisms or requirements for data
sharing.

### Federator of a Gaia-X Ecosystem

A GAIA-X Ecosystem is approved if all Federators comply to GAIA-X 
Policy Rules, and Federation Services fulfil certain criteria (e.g., interoperability 
verified by a testbed). In this case, any entity has the option to
become a Participant and participate in such Ecosystem activities if
they adhere to the Policy Rules.

### Federator of an Ecosystem not federated by Gaia-X AISBL

Federators have the option to facilitate an ecosystem by using the 
available open source Federation Services software but may not be 
officially compliant with Gaia-X Policy Rules. An Ecosystem may, for example, provide
only a private Catalogue and set up its own criteria for having access to
the Ecosystem. Despite this kind of Ecosystem based on Gaia-X
Policy Rules and Services, it cannot be called an official Gaia-X Ecosystem.

### Gaia-X Association AISBL in the Federator role

The Gaia-X Association AISBL may enable and synchronize an Ecosystem. As it is not a
separate entity in the conceptual model, it takes on the role as 
Federator in this case and must comply with the Policy Rules and other
Compliance mechanisms, just as any other Federator is required to do.

## Basic Interactions of Participants

This section describes the basic interaction of the different
Participants as described in the conceptual model (see section 2).

Providers and Consumers within a Ecosystem are identified and well
described through their valid Self-Description, which is initially
created before or during the onboarding process. Providers define their
Service Offerings consisting of Assets and Resources by
Self-Descriptions and publish them in a Catalogue. In turn, Consumers
search for Service Offerings in Gaia-X Catalogues that are coordinated
by Federators. Once the Consumer finds a matching Service Offering in a
Gaia-X Catalogue, the Contract negotiation between Provider and Consumer
determine further conditions under which the Service Instance will be
provided. The Gaia-X association AISBL does not play an intermediary role during the
Contract negotiations but ensures the trustworthiness of all relevant Participants
and Service Offerings.

The following diagram presents the general workflow for Gaia-X service
provisioning and consumption processes. Please note that this overview represents
the current situation and may be subject to changes according to the
Federation Services specification. The specification will provide more
details about the different elements that are part of the concrete
processes.

The Federation Services are visible in the following objects: 

Data Sovereignty Services appear in the mutual agreement and execution of
(Usage) Policies that are defined in a Contract and concern the Data
Asset.

Identity and Trust appears in the onboarding process and ensures the unique
identification of all Participants.

Compliance is also assured during the onboarding and is subject to the 
underlying continuous automated monitoring throughout the lifecycle.

The Federated Catalogue and the Self-Descriptions details the elements that
match Consumers with Providers.

![](figures/image10.png)
*10 Basic Provisioning and Consumption Process*
